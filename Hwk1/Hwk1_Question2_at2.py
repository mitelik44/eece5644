import matplotlib.pyplot as plt # For general plotting

import numpy as np

from scipy.stats import multivariate_normal # MVN not univariate
from sklearn.metrics import confusion_matrix

np.set_printoptions(suppress=True)

# Set seed to generate reproducible "pseudo-randomness" (handles scipy's "randomness" too)
np.random.seed(7)

plt.rc('font', size=22)          # controls default text sizes
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=18)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
plt.rc('ytick', labelsize=14)    # fontsize of the tick labels
plt.rc('legend', fontsize=18)    # legend fontsize
plt.rc('figure', titlesize=22)  # fontsize of the figure title

N = 10000 #number of samples to generate

priors = np.array([0.2, 0.25, 0.25, 0.3])  
# Determine number of classes/mixture components
C = len(priors)

#Gaussian Distributions Means
mu = np.array([[2, 2],
               [7, 7],
               [9, 9],
               [13, 13]])

#Gaussian Distributionns Covariance
Sigma = np.array([[[4, 0],[0, 4]],
                  [[22, 0],[0, 22]],
                  [[7, 0],[0, 7]],
                  [[17, 0],[0, 17]]])

n = mu.shape[1] #determine the number of columns (use 0 to get the # of rows)

#Output samples and labels
X = np.zeros([N, n])
y = np.zeros(N)

#Decide randomly which samples will come from each environment
u = np.random.rand(N)
thresholds = np.cumsum(priors)
print(C)
for c in range(C):
    c_ind = np.argwhere(u <= thresholds[c])[:, 0]  # Get randomly sampled indices for this component
    c_N = len(c_ind)  # No. of samples in this component
    y[c_ind] = c * np.ones(c_N)
    u[c_ind] = 1.1 * np.ones(c_N)  # Multiply by 1.1 to fail <= thresholds and thus not reuse samples
    X[c_ind, :] =  multivariate_normal.rvs(mu[c], Sigma[c], c_N)

# Plot the original data and their true labels
plt.figure(figsize=(10, 10))
plt.plot(X[y==0, 0], X[y==0, 1], 'b.', label="Class 1")
plt.plot(X[y==1, 0], X[y==1, 1], 'rx', label="Class 2")
plt.plot(X[y==2, 0], X[y==2, 1], 'g+', label="Class 3")
plt.plot(X[y==3, 0], X[y==3, 1], 'm*', label="Class 4")
plt.legend()
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.title("Data and True Labels")
plt.tight_layout()
plt.show()

Y = np.array(range(C))
Lambda = np.ones((C,C)) - np.identity(C)
print(Lambda)

# Calculate class-conditional likelihoods p(x|Y=j) for each label of the N observations
class_cond_likelihoods = np.array([multivariate_normal.pdf(X, mu[j], Sigma[j]) for j in Y])
class_priors = np.diag(priors)
print(class_cond_likelihoods.shape)
print(class_priors.shape)
class_posteriors = class_priors.dot(class_cond_likelihoods)
print(class_posteriors)

# We want to create the risk matrix of size 3 x N 
cond_risk = Lambda.dot(class_posteriors)
print(cond_risk)

# Get the decision for each column in risk_mat
decisions = np.argmin(cond_risk, axis=0)
print(decisions.shape)

# Plot for decisions vs true labels
fig = plt.figure(figsize=(10, 10))
marker_shapes = 'ox+*.' # Accomodates up to C=5
#marker_colors = 'brgmy'

# Get sample class counts
sample_class_counts = np.array([sum(y == j) for j in Y])

# Confusion matrix
conf_mat = np.zeros((C, C))
for i in Y: # Each decision option
    for j in Y: # Each class label
        ind_ij = np.argwhere((decisions==i) & (y==j))
        conf_mat[i, j] = len(ind_ij)/sample_class_counts[j] # Average over class sample count

        # True label = Marker shape; Decision = Marker Color
        marker = marker_shapes[j] + 'g'
        plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker)

        if i != j:
            plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker_shapes[j] + 'r')
            
print("Confusion matrix:")
print(conf_mat)

print("Minimum Probability of Error:")
prob_error = 1 - np.diag(conf_mat).dot(sample_class_counts / N)
print(prob_error)

plt.title("Minimum Probability of Error Classified Sampled Data:  {:.3f}".format(prob_error))
plt.show()




###PART B


Lambda = np.array([[0, 1, 2, 3],
               [1, 0, 1, 2],
               [2, 1, 0, 1],
               [3, 2, 1, 0]])

# Calculate class-conditional likelihoods p(x|Y=j) for each label of the N observations
class_cond_likelihoods = np.array([multivariate_normal.pdf(X, mu[j], Sigma[j]) for j in Y])
class_priors = np.diag(priors)
print(class_cond_likelihoods.shape)
print(class_priors.shape)
class_posteriors = class_priors.dot(class_cond_likelihoods)
print(class_posteriors)

# We want to create the risk matrix of size 3 x N 
cond_risk = Lambda.dot(class_posteriors)
print(cond_risk)

# Get the decision for each column in risk_mat
decisions = np.argmin(cond_risk, axis=0)
print(decisions.shape)

# Plot for decisions vs true labels
fig = plt.figure(figsize=(10, 10))
marker_shapes = 'ox+*.' # Accomodates up to C=5
#marker_colors = 'brgmy'

# Get sample class counts
sample_class_counts = np.array([sum(y == j) for j in Y])

# Confusion matrix
conf_mat = np.zeros((C, C))
for i in Y: # Each decision option
    for j in Y: # Each class label
        ind_ij = np.argwhere((decisions==i) & (y==j))
        conf_mat[i, j] = len(ind_ij)/sample_class_counts[j] # Average over class sample count

        # True label = Marker shape; Decision = Marker Color
        marker = marker_shapes[j] + 'g'
        plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker)

        if i != j:
            plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker_shapes[j] + 'r')
            
print("Confusion matrix:")
print(conf_mat)

print("Minimum Probability of Error:")
prob_error = 1 - np.diag(conf_mat).dot(sample_class_counts / N)
print(prob_error)

plt.title("Minimum Probability of Error Classified Sampled Data:  {:.3f}".format(prob_error))
plt.show()


#n = mu.shape[1]

## Class priors
#priors = np.array([0.35, 0.65])  
#C = len(priors)
## Decide randomly which samples will come from each component (taking class 1 from standard normal values above 0.35)
#labels = np.random.rand(N) >= priors[0]
#L = np.array(range(C))
#Nl = np.array([sum(labels == l) for l in L])

## Draw samples from each class pdf
#X = np.zeros((N, n))
#X[labels == 0, :] =  multivariate_normal.rvs(mu[0], Sigma[0], Nl[0])
#X[labels == 1, :] =  multivariate_normal.rvs(mu[1], Sigma[1], Nl[1])

## Expected Risk Minimization Classifier (using true model parameters)
## In practice the parameters would be estimated from training samples
## Using log-likelihood-ratio as the discriminant score for ERM
#class_conditional_likelihoods = np.array([multivariate_normal.pdf(X, mu[l], Sigma[l]) for l in L])
#discriminant_score_erm = np.log(class_conditional_likelihoods[1]) - np.log(class_conditional_likelihoods[0])

## Gamma threshold for MAP decision rule (remove Lambdas and you obtain same gamma on priors only; 0-1 loss simplification)
#gamma_map = (Lambda[1,0] - Lambda[0,0]) / (Lambda[0,1] - Lambda[1,1]) * priors[0]/priors[1]
## Same as:
## gamma_map = priors[0]/priors[1]
#print(gamma_map)

#decisions_map = discriminant_score_erm >= np.log(gamma_map)

## Get indices and probability estimates of the four decision scenarios:
## (true negative, false positive, false negative, true positive)

## True Negative Probability
#ind_00_map = np.argwhere((decisions_map==0) & (labels==0))
#p_00_map = len(ind_00_map) / Nl[0]
## False Positive Probability
#ind_10_map = np.argwhere((decisions_map==1) & (labels==0))
#p_10_map = len(ind_10_map) / Nl[0]
## False Negative Probability
#ind_01_map = np.argwhere((decisions_map==0) & (labels==1))
#p_01_map = len(ind_01_map) / Nl[1]
## True Positive Probability
#ind_11_map = np.argwhere((decisions_map==1) & (labels==1))
#p_11_map = len(ind_11_map) / Nl[1]

## Probability of error for MAP classifier, empirically estimated
#prob_error_erm = np.array((p_10_map, p_01_map)).dot(Nl.T / N)

## Display MAP decisions
#fig = plt.figure(figsize=(10, 10))

## class 0 circle, class 1 +, correct green, incorrect red
#plt.plot(X[ind_00_map, 0], X[ind_00_map, 1], 'og', label="Correct Class 0")
#plt.plot(X[ind_10_map, 0], X[ind_10_map, 1], 'or', label="Incorrect Class 0")
#plt.plot(X[ind_01_map, 0], X[ind_01_map, 1], '+r', label="Incorrect Class 1")
#plt.plot(X[ind_11_map, 0], X[ind_11_map, 1], '+g', label="Correct Class 1")

#plt.legend()
#plt.xlabel(r"$x_1$")
#plt.ylabel(r"$x_2$")
#plt.title("MAP Decisions (RED incorrect)")
#plt.tight_layout()
#plt.show()







## Calculate class-conditional likelihoods p(x|Y=j) for each label of the N observations
#class_cond_likelihoods = np.array([multivariate_normal.pdf(X, mu[j], Sigma[j]) for j in Y])
#class_priors = np.diag(priors)
#print(class_cond_likelihoods.shape)
#print(class_priors.shape)
#class_posteriors = class_priors.dot(class_cond_likelihoods)
#print(class_posteriors)

## We want to create the risk matrix of size 3 x N 
#cond_risk = Lambda.dot(class_posteriors)
#print(cond_risk)

## Get the decision for each column in risk_mat
#decisions = np.argmin(cond_risk, axis=0)
#print(decisions.shape)

## Plot for decisions vs true labels
#fig = plt.figure(figsize=(10, 10))
#marker_shapes = 'ox+*.' # Accomodates up to C=5
#marker_colors = 'brgmy'

## Get sample class counts
#sample_class_counts = np.array([sum(y == j) for j in Y])

## Confusion matrix
#conf_mat = np.zeros((C, C))
#for i in Y: # Each decision option
#    for j in Y: # Each class label
#        ind_ij = np.argwhere((decisions==i) & (y==j))
#        conf_mat[i, j] = len(ind_ij)/sample_class_counts[j] # Average over class sample count

#        # True label = Marker shape; Decision = Marker Color
#        marker = marker_shapes[j] + marker_colors[i]
#        plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker)

#        if i != j:
#            plt.plot(X[ind_ij, 0], X[ind_ij, 1], marker, markersize=16)
            
#print("Confusion matrix:")
#print(conf_mat)

#print("Minimum Probability of Error:")
#prob_error = 1 - np.diag(conf_mat).dot(sample_class_counts / N)
#print(prob_error)

#plt.title("Minimum Probability of Error Classified Sampled Data:  {:.3f}".format(prob_error))
#plt.show()

