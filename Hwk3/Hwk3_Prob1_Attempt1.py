# %%
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcol
from scipy.stats import multivariate_normal
from sys import float_info # Threshold smallest positive floating value
from sklearn.model_selection import cross_val_score, KFold
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
import pandas as pd

import torch
import torch.nn as nn
import torch.nn.functional as F
#from torchsummary import summary

np.set_printoptions(suppress=True)

plt.rc('font', size=22)          # controls default text sizes
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=18)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
plt.rc('ytick', labelsize=14)    # fontsize of the tick labels
plt.rc('legend', fontsize=16)    # legend fontsize
plt.rc('figure', titlesize=22)   # fontsize of the figure title

np.random.seed(7)



def generate_data_from_gmm(N, pdf_params, fig_ax=None):
    # Determine dimensionality from mixture PDF parameters
    n = pdf_params['mu'].shape[1]
    # Determine number of classes/mixture components
    C = len(pdf_params['priors'])

    # Output samples and labels
    X = np.zeros([N, n])
    labels = np.zeros(N)
    
    # Decide randomly which samples will come from each component
    u = np.random.rand(N)
    thresholds = np.cumsum(pdf_params['priors'])
    thresholds = np.insert(thresholds, 0, 0) # For intervals of classes

    Y = np.array(range(1, C+1))
    for y in Y:
        # Get randomly sampled indices for this component
        indices = np.argwhere((thresholds[y-1] <= u) & (u <= thresholds[y]))[:, 0]
        # No. of samples in this component
        Ny = len(indices)  
        labels[indices] = y * np.ones(Ny) - 1
        if n == 1:
            X[indices, 0] =  multivariate_normal(pdf_params['mu'][y-1], pdf_params['Sigma'][y-1], Ny)
        else:
            X[indices, :] =  multivariate_normal.rvs(pdf_params['mu'][y-1], pdf_params['Sigma'][y-1], Ny)

    return X, labels





N = 1000
n = 3
C = 4

gmm_pdf = {}
# Likelihood of each distribution to be selected AND class priors!!!
gmm_pdf['priors'] = np.array([0.25, 0.25, 0.25, 0.25])  
gmm_pdf['mu'] = 1.3*np.array([[1, 1, 1],
                          [-1, 1, 1],
                          [-1, -1, 1],
                          [1, 1, -1]])  # Gaussian distributions means
# gmm_pdf['Sigma'] = np.array([[[2, -0.9],
#                               [-0.9, 1]],
#                              [[2, -0.9],
#                               [-0.9, 3]]])  # Gaussian distributions covariance matrices

gmm_pdf['Sigma'] = np.array([np.eye(3), np.eye(3), np.eye(3), np.eye(3)])

#print(gmm_pdf)

X, labels = generate_data_from_gmm(N, gmm_pdf)
fig = plt.figure()
samples = fig.add_subplot(111, projection='3d')

samples.plot(X[labels == 0, 0], X[labels == 0, 1], X[labels == 0, 2],'rx', label="Class 0", markerfacecolor='none')
samples.plot(X[labels == 1, 0], X[labels == 1, 1], X[labels == 1, 2],'k.', label="Class 1", markerfacecolor='none')
samples.plot(X[labels == 2, 0], X[labels == 2, 1], X[labels == 2, 2],'y^', label="Class 2", markerfacecolor='none')
samples.plot(X[labels == 3, 0], X[labels == 3, 1], X[labels == 3, 2],'go', label="Class 3", markerfacecolor='none')

samples.set_xlabel(r"$x$")
samples.set_ylabel(r"$y$")
samples.set_zlabel(r"$z$")

plt.title("Data and True Labels")
plt.legend()
plt.show()





def perform_erm_classification(X, Lambda, gmm_params, C):
    # ERM classification rule (min prob. of error classifier)    
    # Conditional likelihoods of each x given each class, shape (C, N)
    class_cond_likelihoods = np.array([multivariate_normal.pdf(X, gmm_params['mu'][c], gmm_params['Sigma'][c]) for c in range(C)])

    # Take diag so we have (C, C) shape of priors with prior prob along diagonal
    class_priors = np.diag(gmm_params['priors'])
    # class_priors*likelihood with diagonal matrix creates a matrix of posterior probabilities
    # with each class as a row and N columns for samples, e.g. row 1: [p(y1)p(x1|y1), ..., p(y1)p(xN|y1)]
    class_posteriors = class_priors.dot(class_cond_likelihoods)

    # Conditional risk matrix of size C x N with each class as a row and N columns for samples
    risk_mat = Lambda.dot(class_posteriors)
    
    return np.argmin(risk_mat, axis=0)



N = 100000
X, labels = generate_data_from_gmm(N, gmm_pdf)

# If 0-1 loss then yield MAP decision rule, else ERM classifier
Lambda = np.ones((C, C)) - np.eye(C)

# ERM decision rule, take index/label associated with minimum conditional risk as decision (N, 1)
error = len(np.argwhere(perform_erm_classification(X, Lambda, gmm_pdf, C) != labels))/len(labels)

# Simply using sklearn confusion matrix

conf_mat = confusion_matrix(perform_erm_classification(X, Lambda, gmm_pdf, C), labels)

prob_error = 1 - (np.sum(np.diag(conf_mat)) / N)



#############################################################################################################################################
#############################################################################################################################################
#############################################################################################################################################
#############################################################################################################################################
class TwoLayerMLP(nn.Module):
    # The nn.CrossEntropyLoss() loss function automatically performs a log_softmax() to 
    # the output when validating, on top of calculating the negative-log-likelihood using 
    # nn.NLLLoss(), while also being more stable numerically... So don't implement from scratch
    # https://pytorch.org/docs/stable/generated/torch.nn.Module.html
    
    def __init__(self, input_dim, hidden_dim, C):
        super(TwoLayerMLP, self).__init__()
        # Fully connected layer WX + b mapping from input_dim (n) -> hidden_layer_dim
        self.input_fc = nn.Linear(input_dim, hidden_dim)
        self.relu = nn.ReLU()
        # Output layer again fully connected mapping from hidden_layer_dim -> outputs_dim (C)
        self.output_fc = nn.Linear(hidden_dim, C)
        # Log Softmax (faster and better than straight Softmax)
        # dim=1 refers to the dimension along which the softmax operation is computed
        # In this case computing probabilities across dim 1, i.e., along classes at output la
        self.log_softmax = nn.LogSoftmax(dim=1)

    # Don't call this function directly!! 
    # Simply pass input to model and forward(input) returns output, e.g. model(X)
    def forward(self, x):
        x = self.input_fc(x)  # fc to perceptrons
        x = self.relu(x) # or self.softplus(x) for smooth-ReLU, empirically worse than ReLU
        x = self.output_fc(x)  # connect to output layer
        x = self.log_softmax(x)  # for outputs that sum to 1
        return x


def train_model(model, data, labels, criterion, optimizer, num_epochs=25):
    X_train = torch.FloatTensor(data)
    y_train = torch.LongTensor(labels)

    # Optimize the neural network
    for epoch in range(num_epochs):
        # Set grads to zero explicitly before backprop
        optimizer.zero_grad()
        outputs = model(X_train)
        # Criterion computes the cross entropy loss between input and target
        loss = criterion(outputs, y_train)
        # Backward pass to compute the gradients through the network
        loss.backward()
        # GD step update
        optimizer.step()

    return model


def model_predict(model, data):
    # Similar idea to model.train(), set a flag to let network know your in "inference" mode
    X_test = torch.FloatTensor(data)

    # Evaluate nn on test data and compare to true labels
    predicted_labels = model(X_test)
    # Back to numpy
    predicted_labels = predicted_labels.detach().numpy()
    
    return np.argmax(predicted_labels, 1)


def mse(y_preds, y_true):
    # Residual error (X * theta) - y
    error = y_preds - y_true
    # Loss function is MSE
    return np.mean(error ** 2)


#############################################################################################################################################
#############################################################################################################################################
#############################################################################################################################################
#############################################################################################################################################
fig = plt.figure()


err = np.array([])

xtest, ytest = generate_data_from_gmm(N, gmm_pdf)
# Polynomial degrees ("hyperparameters") to evaluate
degs = np.arange(1, 40, 1) 

N = 100# cha


X_train, y_train = generate_data_from_gmm(N, gmm_pdf)
poly_deg=len(degs)+1
folds=10


kf = KFold(n_splits=folds, shuffle=True)

# Polynomial degrees ("hyperparameters") to evaluate 
degs = np.arange(1, poly_deg, 1)
n_degs = np.max(degs)
error_prob=np.empty((n_degs,folds))
all_models = []
# STEP 2: Try all polynomial orders between 1 (best line fit) and 21 (big time overfit) M=2
for deg in degs:
# K-fold cross validation
    k=0

    # NOTE that these subsets are of the TRAINING dataset
    # Imagine we don't have enough data available to afford another entirely separate validation set
    for fold, (train_indices, valid_indices) in enumerate(kf.split(X_train)):
        # Extract the training and validation sets from the K-fold split
        X_train_k = X_train[train_indices]
        y_train_k = y_train[train_indices]
        X_valid_k = X_train[valid_indices]
        y_valid_k = y_train[valid_indices]
        # Train model parameters
        model = TwoLayerMLP(X_train.shape[1], deg, len(np.unique(y_train)))      
        # Stochastic GD with learning rate and momentum hyperparameters
        optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
        # The nn.CrossEntropyLoss() loss function automatically performs a log_softmax() to 
        # the output when validating, on top of calculating the negative-log-likelihood using 
        # nn.NLLLoss(), while also being more stable numerically... So don't implement from scratch
        criterion = nn.CrossEntropyLoss()
        trainedmodel = train_model(model, X_train_k, y_train_k, criterion, optimizer, num_epochs=200)
        all_models.append(trainedmodel)

        # Make predictions from validation data
        y_valid_pred = model_predict(trainedmodel,X_valid_k)
        num_errors = len(np.argwhere(y_valid_pred != y_valid_k))
        error_prob[deg-1,k] = num_errors/len(y_valid_k)
        k += 1

error_M = np.mean(error_prob, axis=1)

# +1 as the index starts from 0 while the degrees start from 1
optimal_d = np.argmin(error_M) + 1
#print("The model selected to best fit the data without overfitting is: d={}".format(optimal_d))
optimal_hit = error_M[optimal_d-1]

plt.plot(degs,error_M,label=N)

model = TwoLayerMLP(X_train.shape[1], optimal_d, C)
optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
# The nn.CrossEntropyLoss() loss function automatically performs a log_softmax() to 
# the output when validating, on top of calculating the negative log-likelihood using 
# nn.NLLLoss(), while also being more stable numerically... So don't implement from scratch
criterion = nn.CrossEntropyLoss()
trainedmodel = train_model(model, X_train, y_train, criterion, optimizer, num_epochs=200)
y_test_pred = model_predict(trainedmodel,xtest)


num_errors = len(np.argwhere(y_test_pred != ytest))
err.append(num_errors/len(ytest))

# plt.title("P(Error) Training Set")
# plt.legend()
# plt.xlabel("# of Perceptrons")
# plt.ylabel("P-Error")
# plt.show()

# fig = plt.figure()
# plt.scatter(N,err)
# plt.plot(N,err)
# plt.hlines(y=error, xmin = 0, xmax=6000)
# plt.title("Empirically Estimated Test P(error) for each MLP vs. # of Training Samples used in Optimizing")
# plt.legend()
# plt.xscale('log')
# plt.xlabel('Sample Count')
# plt.ylabel("Empirically Estimated P(Error)")
# plt.show()

